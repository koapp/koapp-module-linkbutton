(function() {
  'use strict';

  angular
    .module('linkbutton', [])
    .controller('linkbuttonController', loadFunction);

  loadFunction.$inject = ['$scope', 'structureService', '$location', '$window'];

  function loadFunction($scope, structureService, $location, $window) {
    // Register upper level modules
    structureService.registerModule($location, $scope, 'linkbutton');
    // --- Start linkbuttonController content ---
    var config = $scope.linkbutton.modulescope;

    $scope.menuItems = config.menuItems;
    $scope.showModule = false


    //get all the config styles in {}

    $scope.styles = Object.assign({}, config.color, config.backgroundColor, config.borderStyle, config.borderRadius, config.borderColor, config.borderWidth, config.shadow, config.height, config.width, config.marginLeft, config.marginRight, config.marginTop, config.marginBottom);
    //get position config 
    $scope.position = config.position;

    $scope.styles={
      "color": config.color,
      "background-color":  config.backgroundColor,
      "border-style": config.borderStyle,
      "border-radius": config.borderRadius,
      "border-color": config.borderColor,
      "border-width": config.borderWidth,
      "text-shadow": config.shadow,
      "height": config.height,
      "width": config.width,
      "margin-left": config.marginLeft,
      "margin-right": config.marginRight,
      "margin-bottom": config.marginBottom,
      "margin-top": config.marginTop

    }
    //redirect to config button url oe module
    $scope.redirectButton = function(path){
      $location.path(path);
    };

    //asign then button name from config. If its empty, will take module name
    $scope.menuItems.forEach(function(button){
      
      if (button.linkUrl) {
        $scope.showModule = false;
        button.path = button.linkUrl;
      }else{
        $scope.showModule = true;
        if (!button.name) {
          structureService.getModule(button.path).then(function(module) {
            button.name = module.name;
          });  
        }
      }
    });

    // --- End linkbuttonController content ---
  }
}());
